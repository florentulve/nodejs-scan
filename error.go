package main

var errInvalidArgs = &ErrInvalidArgs{}

// ErrInvalidArgs is raised when the cli arguments are invalid.
type ErrInvalidArgs struct{}

func (e ErrInvalidArgs) Error() string {
	return "Invalid arguments"
}

// ExitCode returns the exit code to be returned by the cli.
func (e ErrInvalidArgs) ExitCode() int {
	return 2
}
